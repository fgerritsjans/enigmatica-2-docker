# engimatica-2-docker

Build from Dockerfile:

``$ docker build -t <docker_user>/enigmatica-2-expert .``

Run headless Docker container:

``$ docker run -it -d <docker_user>/enigmatica-2-expert``